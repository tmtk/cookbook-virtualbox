remote_file node.virtualbox.archive do
  action :create_if_missing
  source node.virtualbox.source
end

case node.platform
when "mac_os_x"
  script "install virtualbox" do
    interpreter "bash"
    code <<-EOF
    hdiutil mount #{node.virtualbox.archive}
    installer -pkg '/Volumes/VirtualBox/VirtualBox.pkg' -target /
    umount '/Volumes/VirtualBox/'
    EOF
  end
when "centos"

  %w[ libGL qt qt-x11 SDL libXmu ].each do |pkg|
    package pkg
  end

  package "virtualbox" do
    source node.virtualbox.archive
    provider Chef::Provider::Package::Rpm
  end
end  
