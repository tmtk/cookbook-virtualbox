default[:virtualbox][:version] = '4.3.2'

default[:virtualbox][:"4.2.16"][:mac_os_x][:source] = "http://download.virtualbox.org/virtualbox/4.2.16/VirtualBox-4.2.16-86992-OSX.dmg"
default[:virtualbox][:"4.2.18"][:mac_os_x][:source] = "http://download.virtualbox.org/virtualbox/4.2.18/VirtualBox-4.2.18-88780-OSX.dmg"
default[:virtualbox][:"4.3.2"][:mac_os_x][:source]  = "http://download.virtualbox.org/virtualbox/4.3.2/VirtualBox-4.3.2-90405-OSX.dmg"
default[:virtualbox][:"4.2.16"][:centos][:source]   = "http://download.virtualbox.org/virtualbox/4.2.16/VirtualBox-4.2-4.2.16_86992_el6-1.x86_64.rpm"
default[:virtualbox][:"4.2.18"][:centos][:source]   = "http://download.virtualbox.org/virtualbox/4.2.18/VirtualBox-4.2-4.2.18_88780_el6-1.x86_64.rpm"
default[:virtualbox][:"4.3.2"][:centos][:source]    = "http://download.virtualbox.org/virtualbox/4.3.2/VirtualBox-4.3-4.3.2_90405_el6-1.i686.rpm"
default[:virtualbox][:source] = default.virtualbox[default.virtualbox.version][node.platform].source

default[:virtualbox][:mac_os_x][:archive] = "#{ENV['HOME']}/Downloads/#{File.basename default.virtualbox.source}"
default[:virtualbox][:centos][:archive]   = "/tmp/#{File.basename default.virtualbox.source}"
default[:virtualbox][:archive] = default.virtualbox[node.platform].archive
